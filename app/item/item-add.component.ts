import { Component, OnInit } from "@angular/core";

import { Item } from "./item";
import { ItemService } from "./item.service";

@Component({
    selector: "add-details",
    moduleId: module.id,
    templateUrl: "./item-add.component.html",
})
export class ItemAddComponent implements OnInit {

    newItem: Item;

    constructor(public itemService: ItemService) { 
        this.newItem = new Item();
    }

    ngOnInit(): void {
    }

    saveItem(){
        this.itemService.saveItem(this.newItem);
        alert("Item Saved");
    }
}
